# README #

# Rampant Slug Pinball Framework #

The RS Pinball Framework has a large focus on ease of configuration and debugging of a pinball machine. The design allows for the Pinball software (Server) running on the computer in the pinball machine to be remotely monitored and configured from a separate computer (client). The Server/Client relationship requires RabbitMQ to be installed on the Server.

NOTE: Currently the software will die silently if RabbitMQ is not configured correctly.
Both the Server and Client applications are being updated to detect a comms failure (or missing software) and function in a standalone mode. 

## How do I run Rampant Slug Framework on a Pinball Machine (server)? ##
* Install Pyprocgame (Only if you are wanting to use the physical PROC Hardware - it requires libpinproc which will install as part of this process)
* [Install Latest Server Executable and ServerLibrary](https://bitbucket.org/rampantslug/pinball-framework/downloads).

### How do I configure server to allow control from the Client software? ###
* [Install Erlang](http://www.erlang.org/download.html)
* [Install RabbitMQ](http://www.rabbitmq.com/download.html)

## How do I install Client software? ##
* [Install Latest Client Executable](https://bitbucket.org/rampantslug/pinball-framework/downloads)


## Using the Source ##

[Example on how to use Mass Transit and configure Rabbit MQ](http://looselycoupledlabs.com/2014/06/masstransit-publish-subscribe-example/)